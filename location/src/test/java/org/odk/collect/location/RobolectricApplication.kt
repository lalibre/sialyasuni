package org.odk.collect.location

import android.app.Application
import org.controlelectoral.sialyasunishared.data.AppState
import org.controlelectoral.sialyasunishared.data.StateStore

class RobolectricApplication : Application(), StateStore {

    private val appState = AppState()

    override fun getState(): AppState {
        return appState
    }
}
