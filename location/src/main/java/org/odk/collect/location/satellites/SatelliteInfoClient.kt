package org.odk.collect.location.satellites

import org.controlelectoral.sialyasunishared.livedata.NonNullLiveData

interface SatelliteInfoClient {

    val satellitesUsedInLastFix: NonNullLiveData<Int>
}
