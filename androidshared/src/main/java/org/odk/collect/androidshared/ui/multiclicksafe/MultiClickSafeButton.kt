package org.controlelectoral.sialyasunishared.ui.multiclicksafe

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.button.MaterialButton
import org.controlelectoral.sialyasunishared.ui.multiclicksafe.MultiClickGuard.allowClick

class MultiClickSafeButton : MaterialButton {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    )

    override fun performClick(): Boolean {
        return allowClick() && super.performClick()
    }
}
