package org.controlelectoral.sialyasunishared.ui.multiclicksafe

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageButton
import org.controlelectoral.sialyasunishared.ui.multiclicksafe.MultiClickGuard.allowClick

class MultiClickSafeImageButton : AppCompatImageButton {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    )

    override fun performClick(): Boolean {
        return allowClick() && super.performClick()
    }
}
