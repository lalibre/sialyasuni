package org.controlelectoral.sialyasunishared.network

import android.net.NetworkInfo

interface NetworkStateProvider {
    val isDeviceOnline: Boolean
    val networkInfo: NetworkInfo?
}
