package org.controlelectoral.sialyasunishared.system

import android.os.Bundle

interface SavedInstanceStateProvider {
    fun getState(savedInstanceState: Bundle?): Bundle?
}
