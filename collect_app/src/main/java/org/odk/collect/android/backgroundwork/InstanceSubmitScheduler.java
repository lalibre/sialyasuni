package org.controlelectoral.sialyasuni.backgroundwork;

public interface InstanceSubmitScheduler {

    void scheduleSubmit(String projectId);

    void cancelSubmit(String projectId);
}
