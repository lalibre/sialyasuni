package org.controlelectoral.sialyasuni.utilities

import android.content.Context
import org.controlelectoral.sialyasuni.database.instances.DatabaseInstancesRepository
import org.controlelectoral.sialyasuni.storage.StoragePathProvider
import org.controlelectoral.sialyasuni.storage.StorageSubdirectory
import org.odk.collect.forms.instances.InstancesRepository

class InstancesRepositoryProvider @JvmOverloads constructor(
    private val context: Context,
    private val storagePathProvider: StoragePathProvider = StoragePathProvider()
) {

    @JvmOverloads
    fun get(projectId: String? = null): InstancesRepository {
        return DatabaseInstancesRepository(
            context,
            storagePathProvider.getOdkDirPath(StorageSubdirectory.METADATA, projectId),
            storagePathProvider.getOdkDirPath(StorageSubdirectory.INSTANCES, projectId),
            System::currentTimeMillis
        )
    }
}
