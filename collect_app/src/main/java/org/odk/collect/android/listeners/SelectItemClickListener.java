package org.controlelectoral.sialyasuni.listeners;

public interface SelectItemClickListener {
    void onItemClicked();
}
