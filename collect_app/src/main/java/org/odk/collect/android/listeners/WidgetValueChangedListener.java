package org.controlelectoral.sialyasuni.listeners;

import org.controlelectoral.sialyasuni.widgets.QuestionWidget;

public interface WidgetValueChangedListener {
    void widgetValueChanged(QuestionWidget changedWidget);
}
