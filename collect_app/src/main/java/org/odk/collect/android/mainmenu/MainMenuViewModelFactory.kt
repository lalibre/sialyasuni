package org.controlelectoral.sialyasuni.mainmenu

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.controlelectoral.sialyasuni.application.initialization.AnalyticsInitializer
import org.controlelectoral.sialyasuni.formmanagement.InstancesAppState
import org.controlelectoral.sialyasuni.instancemanagement.autosend.AutoSendSettingsProvider
import org.controlelectoral.sialyasuni.projects.CurrentProjectProvider
import org.controlelectoral.sialyasuni.utilities.FormsRepositoryProvider
import org.controlelectoral.sialyasuni.utilities.InstancesRepositoryProvider
import org.controlelectoral.sialyasuni.version.VersionInformation
import org.odk.collect.async.Scheduler
import org.odk.collect.permissions.PermissionsChecker
import org.odk.collect.settings.SettingsProvider

open class MainMenuViewModelFactory(
    private val versionInformation: VersionInformation,
    private val application: Application,
    private val settingsProvider: SettingsProvider,
    private val instancesAppState: InstancesAppState,
    private val scheduler: Scheduler,
    private val currentProjectProvider: CurrentProjectProvider,
    private val analyticsInitializer: AnalyticsInitializer,
    private val permissionChecker: PermissionsChecker,
    private val formsRepositoryProvider: FormsRepositoryProvider,
    private val instancesRepositoryProvider: InstancesRepositoryProvider,
    private val autoSendSettingsProvider: AutoSendSettingsProvider
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass) {
            MainMenuViewModel::class.java -> MainMenuViewModel(
                application,
                versionInformation,
                settingsProvider,
                instancesAppState,
                scheduler,
                formsRepositoryProvider,
                instancesRepositoryProvider,
                autoSendSettingsProvider
            )

            CurrentProjectViewModel::class.java -> CurrentProjectViewModel(
                currentProjectProvider,
                analyticsInitializer
            )

            RequestPermissionsViewModel::class.java -> RequestPermissionsViewModel(
                settingsProvider,
                permissionChecker
            )

            else -> throw IllegalArgumentException()
        } as T
    }
}
