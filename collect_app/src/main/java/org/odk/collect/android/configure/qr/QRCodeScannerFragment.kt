package org.controlelectoral.sialyasuni.configure.qr

import android.content.Context
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeResult
import org.odk.collect.analytics.Analytics
import org.controlelectoral.sialyasuni.R
import org.controlelectoral.sialyasuni.activities.ActivityUtils
import org.controlelectoral.sialyasuni.analytics.AnalyticsEvents
import org.controlelectoral.sialyasuni.fragments.BarCodeScannerFragment
import org.controlelectoral.sialyasuni.injection.DaggerUtils
import org.controlelectoral.sialyasuni.mainmenu.MainMenuActivity
import org.controlelectoral.sialyasuni.projects.CurrentProjectProvider
import org.controlelectoral.sialyasuni.storage.StoragePathProvider
import org.controlelectoral.sialyasunishared.ui.ToastUtils.showLongToast
import org.controlelectoral.sialyasunishared.utils.CompressionUtils
import org.odk.collect.settings.ODKAppSettingsImporter
import org.odk.collect.settings.importing.SettingsImportingResult
import java.io.File
import java.io.IOException
import java.util.zip.DataFormatException
import javax.inject.Inject

class QRCodeScannerFragment : BarCodeScannerFragment() {

    @Inject
    lateinit var settingsImporter: ODKAppSettingsImporter

    @Inject
    lateinit var currentProjectProvider: CurrentProjectProvider

    @Inject
    lateinit var storagePathProvider: StoragePathProvider

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerUtils.getComponent(context).inject(this)
    }

    @Throws(IOException::class, DataFormatException::class)
    override fun handleScanningResult(result: BarcodeResult) {
        val oldProjectName = currentProjectProvider.getCurrentProject().name

        val settingsImportingResult = settingsImporter.fromJSON(
            CompressionUtils.decompress(result.text),
            currentProjectProvider.getCurrentProject()
        )

        when (settingsImportingResult) {
            SettingsImportingResult.SUCCESS -> {
                Analytics.log(AnalyticsEvents.RECONFIGURE_PROJECT)

                val newProjectName = currentProjectProvider.getCurrentProject().name
                if (newProjectName != oldProjectName) {
                    File(storagePathProvider.getProjectRootDirPath() + File.separator + oldProjectName).delete()
                    File(storagePathProvider.getProjectRootDirPath() + File.separator + newProjectName).createNewFile()
                }

                showLongToast(
                    requireContext(),
                    getString(org.odk.collect.strings.R.string.successfully_imported_settings)
                )
                ActivityUtils.startActivityAndCloseAllOthers(
                    requireActivity(),
                    MainMenuActivity::class.java
                )
            }

            SettingsImportingResult.INVALID_SETTINGS -> showLongToast(
                requireContext(),
                getString(
                    org.odk.collect.strings.R.string.invalid_qrcode
                )
            )

            SettingsImportingResult.GD_PROJECT -> showLongToast(
                requireContext(),
                getString(org.odk.collect.strings.R.string.settings_with_gd_protocol)
            )
        }
    }

    override fun getSupportedCodeFormats(): Collection<String> {
        return listOf(IntentIntegrator.QR_CODE)
    }
}
