package org.controlelectoral.sialyasuni.formlists.sorting

data class FormListSortingOption(
    val icon: Int,
    val text: Int
)
