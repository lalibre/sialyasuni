package org.controlelectoral.sialyasuni.openrosa;

public interface HttpCredentialsInterface {
    String getUsername();

    String getPassword();
}
