package org.controlelectoral.sialyasuni.formentry.media;

import android.content.Context;

import org.controlelectoral.sialyasuni.audio.AudioHelper;

public interface AudioHelperFactory {

    AudioHelper create(Context context);
}
