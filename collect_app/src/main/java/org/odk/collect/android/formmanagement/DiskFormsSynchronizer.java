package org.controlelectoral.sialyasuni.formmanagement;

public interface DiskFormsSynchronizer {

    void synchronize();
}
