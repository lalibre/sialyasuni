package org.controlelectoral.sialyasuni.itemsets

interface FastExternalItemsetsRepository {

    fun deleteAllByCsvPath(path: String)
}
