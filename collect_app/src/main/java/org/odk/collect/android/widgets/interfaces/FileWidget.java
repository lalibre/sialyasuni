package org.controlelectoral.sialyasuni.widgets.interfaces;

/**
 * @author James Knight
 */
public interface FileWidget extends Widget {
    void deleteFile();
}
