package org.controlelectoral.sialyasuni.database.itemsets

import org.controlelectoral.sialyasuni.fastexternalitemset.ItemsetDbAdapter
import org.controlelectoral.sialyasuni.itemsets.FastExternalItemsetsRepository

class DatabaseFastExternalItemsetsRepository : FastExternalItemsetsRepository {

    override fun deleteAllByCsvPath(path: String) {
        ItemsetDbAdapter().open().use {
            it.delete(path)
        }
    }
}
