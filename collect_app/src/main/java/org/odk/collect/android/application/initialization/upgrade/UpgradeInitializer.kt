package org.controlelectoral.sialyasuni.application.initialization.upgrade

import android.content.Context
import org.controlelectoral.sialyasuni.BuildConfig
import org.controlelectoral.sialyasuni.application.initialization.ExistingProjectMigrator
import org.controlelectoral.sialyasuni.application.initialization.ExistingSettingsMigrator
import org.controlelectoral.sialyasuni.application.initialization.FormUpdatesUpgrade
import org.odk.collect.settings.SettingsProvider
import org.odk.collect.settings.keys.MetaKeys
import org.odk.collect.upgrade.AppUpgrader

class UpgradeInitializer(
    private val context: Context,
    private val settingsProvider: SettingsProvider,
    private val existingProjectMigrator: ExistingProjectMigrator,
    private val existingSettingsMigrator: ExistingSettingsMigrator,
    private val formUpdatesUpgrade: FormUpdatesUpgrade
) {

    fun initialize() {
        AppUpgrader(
            MetaKeys.LAST_LAUNCHED,
            settingsProvider.getMetaSettings(),
            BuildConfig.VERSION_CODE,
            BeforeProjectsInstallDetector(context),
            listOf(
                existingProjectMigrator,
                existingSettingsMigrator,
                formUpdatesUpgrade
            )
        ).upgradeIfNeeded()
    }
}
