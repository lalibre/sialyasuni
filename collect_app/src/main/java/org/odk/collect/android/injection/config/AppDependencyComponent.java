package org.controlelectoral.sialyasuni.injection.config;

import android.app.Application;

import org.javarosa.core.reference.ReferenceManager;
import org.controlelectoral.sialyasuni.activities.AboutActivity;
import org.controlelectoral.sialyasuni.activities.AppListActivity;
import org.controlelectoral.sialyasuni.activities.DeleteSavedFormActivity;
import org.controlelectoral.sialyasuni.activities.FirstLaunchActivity;
import org.controlelectoral.sialyasuni.activities.FormDownloadListActivity;
import org.controlelectoral.sialyasuni.activities.FormFillingActivity;
import org.controlelectoral.sialyasuni.activities.FormHierarchyActivity;
import org.controlelectoral.sialyasuni.activities.FormMapActivity;
import org.controlelectoral.sialyasuni.activities.InstanceChooserList;
import org.controlelectoral.sialyasuni.activities.InstanceUploaderActivity;
import org.controlelectoral.sialyasuni.activities.InstanceUploaderListActivity;
import org.controlelectoral.sialyasuni.adapters.InstanceUploaderAdapter;
import org.controlelectoral.sialyasuni.application.Collect;
import org.controlelectoral.sialyasuni.application.initialization.ApplicationInitializer;
import org.controlelectoral.sialyasuni.application.initialization.ExistingProjectMigrator;
import org.controlelectoral.sialyasuni.audio.AudioRecordingControllerFragment;
import org.controlelectoral.sialyasuni.audio.AudioRecordingErrorDialogFragment;
import org.controlelectoral.sialyasuni.backgroundwork.AutoSendTaskSpec;
import org.controlelectoral.sialyasuni.backgroundwork.AutoUpdateTaskSpec;
import org.controlelectoral.sialyasuni.backgroundwork.SyncFormsTaskSpec;
import org.controlelectoral.sialyasuni.configure.qr.QRCodeScannerFragment;
import org.controlelectoral.sialyasuni.configure.qr.QRCodeTabsActivity;
import org.controlelectoral.sialyasuni.configure.qr.ShowQRCodeFragment;
import org.controlelectoral.sialyasuni.draw.DrawActivity;
import org.controlelectoral.sialyasuni.draw.PenColorPickerDialog;
import org.controlelectoral.sialyasuni.entities.EntitiesRepositoryProvider;
import org.controlelectoral.sialyasuni.external.AndroidShortcutsActivity;
import org.controlelectoral.sialyasuni.external.FormUriActivity;
import org.controlelectoral.sialyasuni.external.FormsProvider;
import org.controlelectoral.sialyasuni.external.InstanceProvider;
import org.controlelectoral.sialyasuni.formentry.BackgroundAudioPermissionDialogFragment;
import org.controlelectoral.sialyasuni.formentry.ODKView;
import org.controlelectoral.sialyasuni.formentry.repeats.DeleteRepeatDialogFragment;
import org.controlelectoral.sialyasuni.formentry.saving.SaveAnswerFileErrorDialogFragment;
import org.controlelectoral.sialyasuni.formentry.saving.SaveFormProgressDialogFragment;
import org.controlelectoral.sialyasuni.formlists.blankformlist.BlankFormListActivity;
import org.controlelectoral.sialyasuni.formmanagement.FormSourceProvider;
import org.controlelectoral.sialyasuni.formmanagement.InstancesAppState;
import org.controlelectoral.sialyasuni.formmanagement.matchexactly.SyncStatusAppState;
import org.controlelectoral.sialyasuni.fragments.AppListFragment;
import org.controlelectoral.sialyasuni.fragments.BarCodeScannerFragment;
import org.controlelectoral.sialyasuni.fragments.SavedFormListFragment;
import org.controlelectoral.sialyasuni.fragments.dialogs.FormsDownloadResultDialog;
import org.controlelectoral.sialyasuni.fragments.dialogs.SelectMinimalDialog;
import org.controlelectoral.sialyasuni.gdrive.GoogleDriveActivity;
import org.controlelectoral.sialyasuni.gdrive.GoogleSheetsUploaderActivity;
import org.controlelectoral.sialyasuni.geo.GoogleMapFragment;
import org.controlelectoral.sialyasuni.mainmenu.MainMenuActivity;
import org.controlelectoral.sialyasuni.openrosa.OpenRosaHttpInterface;
import org.controlelectoral.sialyasuni.preferences.CaptionedListPreference;
import org.controlelectoral.sialyasuni.preferences.dialogs.AdminPasswordDialogFragment;
import org.controlelectoral.sialyasuni.preferences.dialogs.ChangeAdminPasswordDialog;
import org.controlelectoral.sialyasuni.preferences.dialogs.ResetDialogPreferenceFragmentCompat;
import org.controlelectoral.sialyasuni.preferences.dialogs.ServerAuthDialogFragment;
import org.controlelectoral.sialyasuni.preferences.screens.BaseAdminPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.BasePreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.BaseProjectPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.ExperimentalPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.FormManagementPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.FormMetadataPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.IdentityPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.MapsPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.ProjectDisplayPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.ProjectManagementPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.ProjectPreferencesActivity;
import org.controlelectoral.sialyasuni.preferences.screens.ProjectPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.ServerPreferencesFragment;
import org.controlelectoral.sialyasuni.preferences.screens.UserInterfacePreferencesFragment;
import org.controlelectoral.sialyasuni.projects.CurrentProjectProvider;
import org.controlelectoral.sialyasuni.projects.ManualProjectCreatorDialog;
import org.controlelectoral.sialyasuni.projects.ProjectSettingsDialog;
import org.controlelectoral.sialyasuni.projects.QrCodeProjectCreatorDialog;
import org.controlelectoral.sialyasuni.storage.StoragePathProvider;
import org.controlelectoral.sialyasuni.tasks.InstanceServerUploaderTask;
import org.controlelectoral.sialyasuni.tasks.MediaLoadingTask;
import org.controlelectoral.sialyasuni.upload.InstanceUploader;
import org.controlelectoral.sialyasuni.utilities.AuthDialogUtility;
import org.controlelectoral.sialyasuni.utilities.FormsRepositoryProvider;
import org.controlelectoral.sialyasuni.utilities.InstancesRepositoryProvider;
import org.controlelectoral.sialyasuni.utilities.ProjectResetter;
import org.controlelectoral.sialyasuni.utilities.ThemeUtils;
import org.controlelectoral.sialyasuni.widgets.ExStringWidget;
import org.controlelectoral.sialyasuni.widgets.QuestionWidget;
import org.controlelectoral.sialyasuni.widgets.items.SelectOneFromMapDialogFragment;
import org.controlelectoral.sialyasunishared.network.NetworkStateProvider;
import org.odk.collect.async.Scheduler;
import org.odk.collect.location.LocationClient;
import org.odk.collect.maps.MapFragmentFactory;
import org.odk.collect.maps.layers.ReferenceLayerRepository;
import org.odk.collect.permissions.PermissionsChecker;
import org.odk.collect.permissions.PermissionsProvider;
import org.odk.collect.projects.ProjectsRepository;
import org.odk.collect.settings.ODKAppSettingsImporter;
import org.odk.collect.settings.SettingsProvider;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

/**
 * Dagger component for the application. Should include
 * application level Dagger Modules and be built with Application
 * object.
 * <p>
 * Add an `inject(MyClass myClass)` method here for objects you want
 * to inject into so Dagger knows to wire it up.
 * <p>
 * Annotated with @Singleton so modules can include @Singletons that will
 * be retained at an application level (as this an instance of this components
 * is owned by the Application object).
 * <p>
 * If you need to call a provider directly from the component (in a test
 * for example) you can add a method with the type you are looking to fetch
 * (`MyType myType()`) to this interface.
 * <p>
 * To read more about Dagger visit: https://google.github.io/dagger/users-guide
 **/

@Singleton
@Component(modules = {
        AppDependencyModule.class
})
public interface AppDependencyComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        Builder appDependencyModule(AppDependencyModule testDependencyModule);

        AppDependencyComponent build();
    }

    void inject(Collect collect);

    void inject(AboutActivity aboutActivity);

    void inject(InstanceUploaderAdapter instanceUploaderAdapter);

    void inject(SavedFormListFragment savedFormListFragment);

    void inject(FormFillingActivity formFillingActivity);

    void inject(InstanceServerUploaderTask uploader);

    void inject(ServerPreferencesFragment serverPreferencesFragment);

    void inject(ProjectDisplayPreferencesFragment projectDisplayPreferencesFragment);

    void inject(ProjectManagementPreferencesFragment projectManagementPreferencesFragment);

    void inject(AuthDialogUtility authDialogUtility);

    void inject(FormDownloadListActivity formDownloadListActivity);

    void inject(InstanceUploaderListActivity activity);

    void inject(GoogleDriveActivity googleDriveActivity);

    void inject(GoogleSheetsUploaderActivity googleSheetsUploaderActivity);

    void inject(QuestionWidget questionWidget);

    void inject(ExStringWidget exStringWidget);

    void inject(ODKView odkView);

    void inject(FormMetadataPreferencesFragment formMetadataPreferencesFragment);

    void inject(FormMapActivity formMapActivity);

    void inject(GoogleMapFragment mapFragment);

    void inject(MainMenuActivity mainMenuActivity);

    void inject(QRCodeTabsActivity qrCodeTabsActivity);

    void inject(ShowQRCodeFragment showQRCodeFragment);

    void inject(AutoSendTaskSpec autoSendTaskSpec);

    void inject(AdminPasswordDialogFragment adminPasswordDialogFragment);

    void inject(FormHierarchyActivity formHierarchyActivity);

    void inject(FormManagementPreferencesFragment formManagementPreferencesFragment);

    void inject(IdentityPreferencesFragment identityPreferencesFragment);

    void inject(UserInterfacePreferencesFragment userInterfacePreferencesFragment);

    void inject(SaveFormProgressDialogFragment saveFormProgressDialogFragment);

    void inject(BarCodeScannerFragment barCodeScannerFragment);

    void inject(QRCodeScannerFragment qrCodeScannerFragment);

    void inject(ProjectPreferencesActivity projectPreferencesActivity);

    void inject(ResetDialogPreferenceFragmentCompat resetDialogPreferenceFragmentCompat);

    void inject(SyncFormsTaskSpec syncWork);

    void inject(ExperimentalPreferencesFragment experimentalPreferencesFragment);

    void inject(AutoUpdateTaskSpec autoUpdateTaskSpec);

    void inject(ServerAuthDialogFragment serverAuthDialogFragment);

    void inject(BasePreferencesFragment basePreferencesFragment);

    void inject(InstanceUploaderActivity instanceUploaderActivity);

    void inject(ProjectPreferencesFragment projectPreferencesFragment);

    void inject(DeleteSavedFormActivity deleteSavedFormActivity);

    void inject(SelectMinimalDialog selectMinimalDialog);

    void inject(AudioRecordingControllerFragment audioRecordingControllerFragment);

    void inject(SaveAnswerFileErrorDialogFragment saveAnswerFileErrorDialogFragment);

    void inject(AudioRecordingErrorDialogFragment audioRecordingErrorDialogFragment);

    void inject(InstanceChooserList instanceChooserList);

    void inject(FormsProvider formsProvider);

    void inject(InstanceProvider instanceProvider);

    void inject(BackgroundAudioPermissionDialogFragment backgroundAudioPermissionDialogFragment);

    void inject(AppListFragment appListFragment);

    void inject(ChangeAdminPasswordDialog changeAdminPasswordDialog);

    void inject(MediaLoadingTask mediaLoadingTask);

    void inject(ThemeUtils themeUtils);

    void inject(BaseProjectPreferencesFragment baseProjectPreferencesFragment);

    void inject(BaseAdminPreferencesFragment baseAdminPreferencesFragment);

    void inject(CaptionedListPreference captionedListPreference);

    void inject(AndroidShortcutsActivity androidShortcutsActivity);

    void inject(ProjectSettingsDialog projectSettingsDialog);

    void inject(ManualProjectCreatorDialog manualProjectCreatorDialog);

    void inject(QrCodeProjectCreatorDialog qrCodeProjectCreatorDialog);

    void inject(FirstLaunchActivity firstLaunchActivity);

    void inject(InstanceUploader instanceUploader);

    void inject(FormUriActivity formUriActivity);

    void inject(MapsPreferencesFragment mapsPreferencesFragment);

    void inject(FormsDownloadResultDialog formsDownloadResultDialog);

    void inject(SelectOneFromMapDialogFragment selectOneFromMapDialogFragment);

    void inject(DrawActivity drawActivity);

    void inject(PenColorPickerDialog colorPickerDialog);

    void inject(BlankFormListActivity blankFormListActivity);

    void inject(DeleteRepeatDialogFragment deleteRepeatDialogFragment);

    void inject(AppListActivity appListActivity);

    OpenRosaHttpInterface openRosaHttpInterface();

    ReferenceManager referenceManager();

    SettingsProvider settingsProvider();

    ApplicationInitializer applicationInitializer();

    ODKAppSettingsImporter settingsImporter();

    ProjectsRepository projectsRepository();

    CurrentProjectProvider currentProjectProvider();

    InstancesAppState instancesAppState();

    StoragePathProvider storagePathProvider();

    FormsRepositoryProvider formsRepositoryProvider();

    InstancesRepositoryProvider instancesRepositoryProvider();

    SyncStatusAppState syncStatusAppState();

    FormSourceProvider formSourceProvider();

    ExistingProjectMigrator existingProjectMigrator();

    ProjectResetter projectResetter();

    MapFragmentFactory mapFragmentFactory();

    Scheduler scheduler();

    LocationClient locationClient();

    PermissionsProvider permissionsProvider();

    PermissionsChecker permissionsChecker();

    ReferenceLayerRepository referenceLayerRepository();

    NetworkStateProvider networkStateProvider();

    EntitiesRepositoryProvider entitiesRepositoryProvider();
}
