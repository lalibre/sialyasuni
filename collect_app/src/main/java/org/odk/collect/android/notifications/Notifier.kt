package org.controlelectoral.sialyasuni.notifications

import org.controlelectoral.sialyasuni.formmanagement.FormDownloadException
import org.controlelectoral.sialyasuni.formmanagement.ServerFormDetails
import org.controlelectoral.sialyasuni.upload.FormUploadException
import org.odk.collect.forms.FormSourceException
import org.odk.collect.forms.instances.Instance

interface Notifier {
    fun onUpdatesAvailable(updates: List<ServerFormDetails>, projectId: String)
    fun onUpdatesDownloaded(result: Map<ServerFormDetails, FormDownloadException?>, projectId: String)
    fun onSync(exception: FormSourceException?, projectId: String)
    fun onSubmission(result: Map<Instance, FormUploadException?>, projectId: String)
}
