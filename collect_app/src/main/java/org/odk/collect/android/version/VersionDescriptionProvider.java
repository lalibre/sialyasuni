package org.controlelectoral.sialyasuni.version;

public interface VersionDescriptionProvider {
    String getVersionDescription();
}