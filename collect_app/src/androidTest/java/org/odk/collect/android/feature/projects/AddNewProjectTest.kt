package org.controlelectoral.sialyasuni.feature.projects

import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.controlelectoral.sialyasuni.R
import org.controlelectoral.sialyasuni.support.TestDependencies
import org.controlelectoral.sialyasuni.support.pages.MainMenuPage
import org.controlelectoral.sialyasuni.support.rules.CollectTestRule
import org.controlelectoral.sialyasuni.support.rules.TestRuleChain
import org.controlelectoral.sialyasunitest.RecordedIntentsRule

class AddNewProjectTest {

    val rule = CollectTestRule()
    private val testDependencies = TestDependencies()

    @get:Rule
    val chain: RuleChain = TestRuleChain.chain(testDependencies)
        .around(RecordedIntentsRule())
        .around(rule)

    @Test
    fun addingProjectManually_addsNewProject() {
        rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickAddProject()
            .switchToManualMode()
            .inputUrl("https://my-server.com")
            .inputUsername("John")
            .addProject()

            .openProjectSettingsDialog()
            .assertCurrentProject("my-server.com", "John / my-server.com")
            .assertInactiveProject("CONTROL ELECTORAL", "demo.getodk.org")
    }

    @Test
    fun addingProjectFromQrCode_addsNewProject() {
        val page = rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickAddProject()

        testDependencies.stubBarcodeViewDecoder.scan("{\"general\":{\"server_url\":\"https:\\/\\/my-server.com\",\"username\":\"adam\",\"password\":\"1234\"},\"admin\":{}}")
        page.checkIsToastWithMessageDisplayed(org.odk.collect.strings.R.string.switched_project, "my-server.com")

        MainMenuPage()
            .assertOnPage()
            .openProjectSettingsDialog()
            .assertCurrentProject("my-server.com", "adam / my-server.com")
            .assertInactiveProject("CONTROL ELECTORAL", "demo.getodk.org")
    }

    @Test
    fun switchesToExistingProject_whenDuplicateProjectScanned_andOptionToSwitchToExistingSelected() {
        val page = rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickAddProject()

        testDependencies.stubBarcodeViewDecoder.scan("{\"general\":{\"server_url\":\"https://demo.getodk.org\"},\"admin\":{}}")

        page.assertDuplicateDialogShown()
            .switchToExistingProject()
            .checkIsToastWithMessageDisplayed(org.odk.collect.strings.R.string.switched_project, "CONTROL ELECTORAL")
            .openProjectSettingsDialog()
            .assertCurrentProject("CONTROL ELECTORAL", "demo.getodk.org")
            .assertNotInactiveProject("CONTROL ELECTORAL")
    }

    @Test
    fun addsDuplicateProject_whenDuplicateProjectScanned_andOptionToAddDuplicateProjectSelected() {
        val page = rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickAddProject()

        testDependencies.stubBarcodeViewDecoder.scan("{\"general\":{\"server_url\":\"https://demo.getodk.org\"},\"admin\":{}}")

        page.assertDuplicateDialogShown()
            .addDuplicateProject()
            .checkIsToastWithMessageDisplayed(org.odk.collect.strings.R.string.switched_project, "CONTROL ELECTORAL")
            .openProjectSettingsDialog()
            .assertCurrentProject("CONTROL ELECTORAL", "demo.getodk.org")
            .assertInactiveProject("CONTROL ELECTORAL", "demo.getodk.org")
    }

    @Test
    fun switchesToExistingProject_whenDuplicateProjectEnteredManually_andOptionToSwitchToExistingSelected() {
        rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickAddProject()
            .switchToManualMode()
            .inputUrl("https://demo.getodk.org")
            .addProjectAndAssertDuplicateDialogShown()
            .switchToExistingProject()
            .checkIsToastWithMessageDisplayed(org.odk.collect.strings.R.string.switched_project, "CONTROL ELECTORAL")
            .openProjectSettingsDialog()
            .assertCurrentProject("CONTROL ELECTORAL", "demo.getodk.org")
            .assertNotInactiveProject("CONTROL ELECTORAL")
    }

    @Test
    fun addsDuplicateProject_whenDuplicateProjectEnteredManually_andOptionToAddDuplicateProjectSelected() {
        rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickAddProject()
            .switchToManualMode()
            .inputUrl("https://demo.getodk.org")
            .addProjectAndAssertDuplicateDialogShown()
            .addDuplicateProject()
            .checkIsToastWithMessageDisplayed(org.odk.collect.strings.R.string.switched_project, "CONTROL ELECTORAL")
            .openProjectSettingsDialog()
            .assertCurrentProject("CONTROL ELECTORAL", "demo.getodk.org")
            .assertInactiveProject("CONTROL ELECTORAL", "demo.getodk.org")
    }
}
