package org.controlelectoral.sialyasuni.feature.projects

import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.controlelectoral.sialyasuni.support.pages.MainMenuPage
import org.controlelectoral.sialyasuni.support.pages.ProjectSettingsPage
import org.controlelectoral.sialyasuni.support.rules.CollectTestRule
import org.controlelectoral.sialyasuni.support.rules.TestRuleChain

class UpdateProjectTest {

    val rule = CollectTestRule()

    @get:Rule
    var chain: RuleChain = TestRuleChain
        .chain()
        .around(rule)

    @Test
    fun updateProjectTest() {
        rule.startAtMainMenu()
            .assertProjectIcon("D")
            .openProjectSettingsDialog()
            .assertCurrentProject("CONTROL ELECTORAL", "demo.getodk.org")
            .clickSettings()
            .clickProjectDisplay()
            .setProjectName("Project X")
            .assertFileWithProjectNameUpdated("CONTROL ELECTORAL", "Project X")
            .setProjectIcon("XY")
            .setProjectColor("cccccc")
            .pressBack(ProjectSettingsPage())
            .pressBack(MainMenuPage())
            .openProjectSettingsDialog()
            .clickSettings()
            .clickServerSettings()
            .clickServerUsername()
            .inputText("Anna")
            .clickOKOnDialog()
            .pressBack(ProjectSettingsPage())
            .pressBack(MainMenuPage())

            .assertProjectIcon("X")
            .openProjectSettingsDialog()
            .assertCurrentProject("Project X", "Anna / demo.getodk.org")
    }

    @Test
    fun updateProjectName_updatesProjectNameFileSanitizingIt() {
        rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickSettings()
            .clickProjectDisplay()
            .setProjectName("Project<>")
            .assertFileWithProjectNameUpdated("CONTROL ELECTORAL", "Project__")
            .setProjectName(":*Project<>")
            .assertFileWithProjectNameUpdated("Project__", "__Project__")
    }
}
