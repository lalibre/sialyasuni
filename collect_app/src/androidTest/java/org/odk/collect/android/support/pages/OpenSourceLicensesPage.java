package org.controlelectoral.sialyasuni.support.pages;

class OpenSourceLicensesPage extends Page<OpenSourceLicensesPage> {

    @Override
    public OpenSourceLicensesPage assertOnPage() {
        waitForText("Open Source Licenses");
        checkIfWebViewActivityIsDisplayed();
        return this;
    }
}
