package org.controlelectoral.sialyasuni.feature.settings

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith
import org.controlelectoral.sialyasuni.support.rules.CollectTestRule
import org.controlelectoral.sialyasuni.support.rules.TestRuleChain

@RunWith(AndroidJUnit4::class)
class SettingLanguageTest {
    private val rule = CollectTestRule()

    @get:Rule
    var ruleChain: RuleChain = TestRuleChain.chain().around(rule)

    @Test
    fun settingLanguageWithoutCountryCodeShouldLoadProperTranslations() {
        rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickSettings()
            .clickOnUserInterface()
            .clickOnLanguage()
            .clickOnSelectedLanguage("dansk")
            .openProjectSettingsDialog()
            .clickSettings()
            .assertText("Kort")
    }

    @Test
    fun settingLanguageWithCountryCodeShouldLoadProperTranslations() {
        rule.startAtMainMenu()
            .openProjectSettingsDialog()
            .clickSettings()
            .clickOnUserInterface()
            .clickOnLanguage()
            .clickOnSelectedLanguage("svenska (Sverige)")
            .assertText("Fyll i tomt formulär")
    }
}
