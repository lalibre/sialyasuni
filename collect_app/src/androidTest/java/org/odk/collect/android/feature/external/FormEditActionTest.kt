package org.controlelectoral.sialyasuni.feature.external

import android.content.Intent
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.Matchers.equalTo
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith
import org.controlelectoral.sialyasuni.external.FormsContract
import org.controlelectoral.sialyasuni.external.InstancesContract
import org.controlelectoral.sialyasuni.support.ContentProviderUtils
import org.controlelectoral.sialyasuni.support.pages.AppClosedPage
import org.controlelectoral.sialyasuni.support.pages.FormEntryPage
import org.controlelectoral.sialyasuni.support.rules.CollectTestRule
import org.controlelectoral.sialyasuni.support.rules.TestRuleChain

@RunWith(AndroidJUnit4::class)
class FormEditActionTest {

    private val rule = CollectTestRule()

    @get:Rule
    val chain: RuleChain = TestRuleChain.chain()
        .around(rule)

    @Test
    fun editForm_andThenFillingForm_returnsNewInstanceURI() {
        rule.startAtMainMenu()
            .copyAndSyncForm("one-question.xml")

        val formId = ContentProviderUtils.getFormDatabaseId("DEMO", "one_question")
        val uri = FormsContract.getUri("DEMO", formId)

        val formIntent = Intent(Intent.ACTION_EDIT).also { it.data = uri }
        val result = rule.launchForResult(formIntent, FormEntryPage("One Question")) {
            it.answerQuestion("what is your age", "31")
                .swipeToEndScreen()
                .clickFinalize(AppClosedPage())
        }

        val instanceId = ContentProviderUtils.getInstanceDatabaseId("DEMO", "one_question")
        assertThat(result.resultData.data, equalTo(InstancesContract.getUri("DEMO", instanceId)))
    }
}
