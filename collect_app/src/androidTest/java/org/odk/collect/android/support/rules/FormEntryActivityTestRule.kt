package org.controlelectoral.sialyasuni.support.rules

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import org.junit.rules.ExternalResource
import org.controlelectoral.sialyasuni.activities.FormFillingActivity
import org.controlelectoral.sialyasuni.external.FormsContract
import org.controlelectoral.sialyasuni.formmanagement.FormFillingIntentFactory
import org.controlelectoral.sialyasuni.injection.DaggerUtils
import org.controlelectoral.sialyasuni.injection.config.AppDependencyModule
import org.controlelectoral.sialyasuni.storage.StorageSubdirectory
import org.controlelectoral.sialyasuni.support.CollectHelpers
import org.controlelectoral.sialyasuni.support.StorageUtils
import org.controlelectoral.sialyasuni.support.pages.FormEntryPage
import org.controlelectoral.sialyasuni.support.pages.FormHierarchyPage
import org.controlelectoral.sialyasuni.support.pages.Page
import org.controlelectoral.sialyasunishared.system.SavedInstanceStateProvider
import org.controlelectoral.sialyasunitest.ActivityScenarioExtensions.saveInstanceState
import org.odk.collect.projects.Project
import timber.log.Timber
import java.io.IOException

class FormEntryActivityTestRule : ExternalResource() {

    private lateinit var intent: Intent
    private lateinit var scenario: ActivityScenario<Activity>

    private var outState: Bundle? = null

    private val savedInstanceStateProvider = InMemSavedInstanceStateProvider()

    override fun before() {
        super.before()

        CollectHelpers.overrideAppDependencyModule(object : AppDependencyModule() {
            override fun providesSavedInstanceStateProvider(): SavedInstanceStateProvider {
                return savedInstanceStateProvider
            }
        })
    }

    override fun after() {
        try {
            scenario.close()
        } catch (e: Throwable) {
            Timber.e(Error("Error closing ActivityScenario: $e"))
        }
    }

    fun setUpProjectAndCopyForm(formFilename: String): FormEntryActivityTestRule {
        try {
            // Set up CONTROL ELECTORAL
            val component =
                DaggerUtils.getComponent(ApplicationProvider.getApplicationContext<Application>())
            component.projectsRepository().save(Project.DEMO_PROJECT)
            component.currentProjectProvider().setCurrentProject(Project.DEMO_PROJECT_ID)
            StorageUtils.copyFormToDemoProject(formFilename, null, true)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }

        return this
    }

    fun <D : Page<D>> fillNewForm(formFilename: String, destination: D): D {
        intent = createNewFormIntent(formFilename)
        scenario = ActivityScenario.launch(intent)
        return destination.assertOnPage()
    }

    fun fillNewForm(formFilename: String, formName: String): FormEntryPage {
        return fillNewForm(formFilename, FormEntryPage(formName))
    }

    fun editForm(formFilename: String, instanceName: String): FormHierarchyPage {
        intent = createEditFormIntent(formFilename)
        scenario = ActivityScenario.launch(intent)
        return FormHierarchyPage(instanceName).assertOnPage()
    }

    fun navigateAwayFromActivity(): FormEntryActivityTestRule {
        scenario.moveToState(Lifecycle.State.STARTED)
        outState = scenario.saveInstanceState()
        return this
    }

    fun destroyActivity(): FormEntryActivityTestRule {
        scenario.moveToState(Lifecycle.State.DESTROYED)
        return this
    }

    fun restoreActivity() {
        savedInstanceStateProvider.setState(outState)
        scenario = ActivityScenario.launch(intent)
    }

    private fun createNewFormIntent(formFilename: String): Intent {
        val application = ApplicationProvider.getApplicationContext<Application>()
        val formPath = DaggerUtils.getComponent(application).storagePathProvider()
            .getOdkDirPath(StorageSubdirectory.FORMS) + "/" + formFilename
        val form = DaggerUtils.getComponent(application).formsRepositoryProvider().get()
            .getOneByPath(formPath)
        val projectId = DaggerUtils.getComponent(application).currentProjectProvider()
            .getCurrentProject().uuid

        return FormFillingIntentFactory.newInstanceIntent(
            application,
            FormsContract.getUri(projectId, form!!.dbId),
            FormFillingActivity::class
        )
    }

    private fun createEditFormIntent(formFilename: String): Intent {
        val application = ApplicationProvider.getApplicationContext<Application>()
        val formPath = DaggerUtils.getComponent(application).storagePathProvider()
            .getOdkDirPath(StorageSubdirectory.FORMS) + "/" + formFilename
        val form = DaggerUtils.getComponent(application).formsRepositoryProvider().get()
            .getOneByPath(formPath)
        val instance = DaggerUtils.getComponent(application).instancesRepositoryProvider().get()
            .getAllByFormId(form!!.formId).first()
        val projectId = DaggerUtils.getComponent(application).currentProjectProvider()
            .getCurrentProject().uuid

        return FormFillingIntentFactory.editInstanceIntent(
            application,
            projectId,
            instance.dbId,
            FormFillingActivity::class
        )
    }
}

private class InMemSavedInstanceStateProvider : SavedInstanceStateProvider {

    private var bundle: Bundle? = null

    fun setState(savedInstanceState: Bundle?) {
        bundle = savedInstanceState
    }

    override fun getState(savedInstanceState: Bundle?): Bundle? {
        return if (bundle != null) {
            bundle.also {
                bundle = null
            }
        } else {
            savedInstanceState
        }
    }
}
