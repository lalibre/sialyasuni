package org.controlelectoral.sialyasuni.feature.instancemanagement;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;
import org.controlelectoral.sialyasuni.support.rules.CollectTestRule;
import org.controlelectoral.sialyasuni.support.rules.TestRuleChain;
import org.controlelectoral.sialyasuni.support.pages.MainMenuPage;

@RunWith(AndroidJUnit4.class)
public class DeleteSavedFormTest {

    public final CollectTestRule rule = new CollectTestRule();

    @Rule
    public final RuleChain chain = TestRuleChain.chain()
            .around(rule);

    @Test
    public void deletingAForm_removesFormFromFinalizedForms() {
        rule.startAtMainMenu()
                .copyForm("one-question.xml")
                .startBlankForm("One Question")
                .answerQuestion("what is your age", "30")
                .swipeToEndScreen()
                .clickFinalize()

                .clickDeleteSavedForm()
                .clickForm("One Question")
                .clickDeleteSelected(1)
                .clickDeleteForms()
                .assertTextDoesNotExist("One Question")
                .pressBack(new MainMenuPage())
                .assertNumberOfFinalizedForms(0);
    }
}
