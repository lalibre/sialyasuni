package org.controlelectoral.sialyasuni.support.rules

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.controlelectoral.sialyasuni.database.DatabaseConnection.Companion.closeAll
import org.controlelectoral.sialyasuni.injection.DaggerUtils
import org.controlelectoral.sialyasuni.injection.config.AppDependencyComponent
import org.controlelectoral.sialyasuni.injection.config.AppDependencyModule
import org.controlelectoral.sialyasuni.support.CollectHelpers
import org.controlelectoral.sialyasuni.views.DecoratedBarcodeView
import org.controlelectoral.sialyasunishared.data.getState
import org.controlelectoral.sialyasunishared.ui.ToastUtils
import org.controlelectoral.sialyasunishared.ui.multiclicksafe.MultiClickGuard
import org.odk.collect.material.BottomSheetBehavior
import org.odk.collect.shared.files.DirectoryUtils
import java.io.File
import java.io.IOException

private class ResetStateStatement(
    private val base: Statement,
    private val appDependencyModule: AppDependencyModule? = null
) : Statement() {

    override fun evaluate() {
        val application = ApplicationProvider.getApplicationContext<Application>()
        val oldComponent = DaggerUtils.getComponent(application)

        clearPrefs(oldComponent)
        clearDisk(oldComponent)
        setTestState()
        CollectHelpers.simulateProcessRestart(appDependencyModule)
        base.evaluate()
    }

    private fun clearAppState(application: Application) {
        application.getState().clear()
    }

    private fun setTestState() {
        MultiClickGuard.test = true
        DecoratedBarcodeView.test = true
        ToastUtils.recordToasts = true
        BottomSheetBehavior.DRAGGING_ENABLED = false
    }

    private fun clearDisk(component: AppDependencyComponent) {
        try {
            DirectoryUtils.deleteDirectory(File(component.storagePathProvider().odkRootDirPath))
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
        closeAll()
    }

    private fun clearPrefs(component: AppDependencyComponent) {
        val projectIds = component.projectsRepository().getAll().map { it.uuid }
        component.settingsProvider().clearAll(projectIds)
    }
}

class ResetStateRule @JvmOverloads constructor(
    private val appDependencyModule: AppDependencyModule? = null
) : TestRule {

    override fun apply(base: Statement, description: Description): Statement =
        ResetStateStatement(base, appDependencyModule)
}
