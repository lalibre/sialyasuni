package org.controlelectoral.sialyasuni.support

import androidx.fragment.app.FragmentActivity
import org.controlelectoral.sialyasuni.audio.AudioControllerView.SwipableParent

class SwipableParentActivity : FragmentActivity(), SwipableParent {
    var isSwipingAllowed = false
        private set

    override fun allowSwiping(allowSwiping: Boolean) {
        isSwipingAllowed = allowSwiping
    }
}
