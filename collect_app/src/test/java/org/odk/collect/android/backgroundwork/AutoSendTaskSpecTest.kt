package org.controlelectoral.sialyasuni.backgroundwork

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.controlelectoral.sialyasuni.TestSettingsProvider
import org.controlelectoral.sialyasuni.formmanagement.FormSourceProvider
import org.controlelectoral.sialyasuni.formmanagement.InstancesAppState
import org.controlelectoral.sialyasuni.gdrive.GoogleAccountsManager
import org.controlelectoral.sialyasuni.gdrive.GoogleApiProvider
import org.controlelectoral.sialyasuni.injection.config.AppDependencyModule
import org.controlelectoral.sialyasuni.instancemanagement.autosend.AutoSendSettingsProvider
import org.controlelectoral.sialyasuni.instancemanagement.autosend.InstanceAutoSender
import org.controlelectoral.sialyasuni.notifications.Notifier
import org.controlelectoral.sialyasuni.projects.ProjectDependencyProvider
import org.controlelectoral.sialyasuni.projects.ProjectDependencyProviderFactory
import org.controlelectoral.sialyasuni.storage.StoragePathProvider
import org.controlelectoral.sialyasuni.support.CollectHelpers
import org.controlelectoral.sialyasuni.utilities.ChangeLockProvider
import org.controlelectoral.sialyasuni.utilities.FormsRepositoryProvider
import org.controlelectoral.sialyasuni.utilities.InstancesRepositoryProvider
import org.odk.collect.metadata.PropertyManager
import org.odk.collect.permissions.PermissionsProvider
import org.odk.collect.settings.SettingsProvider
import org.odk.collect.settings.keys.ProjectKeys
import org.odk.collect.testshared.RobolectricHelpers

@RunWith(AndroidJUnit4::class)
class AutoSendTaskSpecTest {

    private val instanceAutoSender = mock<InstanceAutoSender>()
    private val projectDependencyProvider = mock<ProjectDependencyProvider>()
    private val projectDependencyProviderFactory = mock<ProjectDependencyProviderFactory>()

    private lateinit var projectId: String

    @Before
    fun setup() {
        CollectHelpers.overrideAppDependencyModule(object : AppDependencyModule() {
            override fun providesInstanceAutoSender(
                autoSendSettingsProvider: AutoSendSettingsProvider?,
                context: Context?,
                notifier: Notifier?,
                googleAccountsManager: GoogleAccountsManager?,
                googleApiProvider: GoogleApiProvider?,
                permissionsProvider: PermissionsProvider?,
                instancesAppState: InstancesAppState?,
                propertyManager: PropertyManager?
            ): InstanceAutoSender {
                return instanceAutoSender
            }

            override fun providesProjectDependencyProviderFactory(
                settingsProvider: SettingsProvider?,
                formsRepositoryProvider: FormsRepositoryProvider?,
                instancesRepositoryProvider: InstancesRepositoryProvider?,
                storagePathProvider: StoragePathProvider?,
                changeLockProvider: ChangeLockProvider?,
                formSourceProvider: FormSourceProvider?
            ): ProjectDependencyProviderFactory {
                return projectDependencyProviderFactory
            }
        })

        RobolectricHelpers.mountExternalStorage()
        projectId = CollectHelpers.setupDemoProject()
        TestSettingsProvider.getUnprotectedSettings(projectId)
            .save(ProjectKeys.KEY_AUTOSEND, "wifi_and_cellular")

        whenever(projectDependencyProviderFactory.create(projectId)).thenReturn(projectDependencyProvider)
    }

    @Test
    fun `passes projectDependencyProvider with proper project id`() {
        val inputData = mapOf(TaskData.DATA_PROJECT_ID to projectId)
        AutoSendTaskSpec().getTask(ApplicationProvider.getApplicationContext(), inputData, true).get()
        verify(instanceAutoSender).autoSendInstances(projectDependencyProvider)
    }

    @Test
    fun `maxRetries should not be limited`() {
        assertThat(AutoSendTaskSpec().maxRetries, `is`(nullValue()))
    }
}
