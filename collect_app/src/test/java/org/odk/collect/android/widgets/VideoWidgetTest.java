package org.controlelectoral.sialyasuni.widgets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;

import androidx.annotation.NonNull;

import net.bytebuddy.utility.RandomString;

import org.javarosa.core.model.data.StringData;
import org.junit.Before;
import org.junit.Test;
import org.controlelectoral.sialyasuni.R;
import org.controlelectoral.sialyasuni.formentry.questions.QuestionDetails;
import org.controlelectoral.sialyasuni.injection.config.AppDependencyModule;
import org.controlelectoral.sialyasuni.support.CollectHelpers;
import org.controlelectoral.sialyasuni.utilities.MediaUtils;
import org.controlelectoral.sialyasuni.widgets.base.FileWidgetTest;
import org.controlelectoral.sialyasuni.widgets.support.FakeQuestionMediaManager;
import org.controlelectoral.sialyasuni.widgets.support.FakeWaitingForDataRegistry;
import org.controlelectoral.sialyasunishared.system.IntentLauncher;
import org.odk.collect.shared.TempFiles;

/**
 * @author James Knight
 */
public class VideoWidgetTest extends FileWidgetTest<VideoWidget> {
    private String destinationName;

    @NonNull
    @Override
    public VideoWidget createWidget() {
        return new VideoWidget(activity, new QuestionDetails(formEntryPrompt, readOnlyOverride), new FakeWaitingForDataRegistry(), new FakeQuestionMediaManager());
    }

    @NonNull
    @Override
    public StringData getNextAnswer() {
        return new StringData(destinationName);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        destinationName = RandomString.make();
    }

    @Test
    public void buttonsShouldLaunchCorrectIntents() {
        MediaUtils mediaUtils = mock(MediaUtils.class);
        CollectHelpers.overrideAppDependencyModule(new AppDependencyModule() {
            @Override
            public MediaUtils providesMediaUtils(IntentLauncher intentLauncher) {
                return mediaUtils;
            }
        });

        stubAllRuntimePermissionsGranted(true);

        Intent intent = getIntentLaunchedByClick(R.id.capture_video);
        assertActionEquals(MediaStore.ACTION_VIDEO_CAPTURE, intent);

        intent = getIntentLaunchedByClick(R.id.choose_video);
        assertActionEquals(Intent.ACTION_GET_CONTENT, intent);
        assertTypeEquals("video/*", intent);

        getWidget().setData(TempFiles.createTempFile(TempFiles.createTempDir()));
        getIntentLaunchedByClick(R.id.play_video);
        verify(mediaUtils).openFile(any(), any(), any());
    }

    @Test
    public void buttonsShouldNotLaunchIntentsWhenPermissionsDenied() {
        stubAllRuntimePermissionsGranted(false);

        assertNull(getIntentLaunchedByClick(R.id.capture_video));
    }

    @Test
    public void usingReadOnlyOptionShouldMakeAllClickableElementsDisabled() {
        when(formEntryPrompt.isReadOnly()).thenReturn(true);

        assertThat(getSpyWidget().captureButton.getVisibility(), is(View.GONE));
        assertThat(getSpyWidget().chooseButton.getVisibility(), is(View.GONE));
        assertThat(getSpyWidget().playButton.getVisibility(), is(View.VISIBLE));
        assertThat(getSpyWidget().playButton.isEnabled(), is(Boolean.FALSE));
        assertThat(getSpyWidget().playButton.getText(), is("Play Video"));
    }

    @Test
    public void whenReadOnlyOverrideOptionIsUsed_shouldAllClickableElementsBeDisabled() {
        readOnlyOverride = true;
        when(formEntryPrompt.isReadOnly()).thenReturn(false);

        assertThat(getSpyWidget().captureButton.getVisibility(), is(View.GONE));
        assertThat(getSpyWidget().chooseButton.getVisibility(), is(View.GONE));
        assertThat(getSpyWidget().playButton.getVisibility(), is(View.VISIBLE));
        assertThat(getSpyWidget().playButton.isEnabled(), is(Boolean.FALSE));
        assertThat(getSpyWidget().playButton.getText(), is("Play Video"));
    }
}
