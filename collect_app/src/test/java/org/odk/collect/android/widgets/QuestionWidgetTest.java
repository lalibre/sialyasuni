package org.controlelectoral.sialyasuni.widgets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.controlelectoral.sialyasuni.support.CollectHelpers.setupFakeReferenceManager;
import static java.util.Arrays.asList;

import android.content.Context;

import androidx.core.util.Pair;
import androidx.lifecycle.MutableLiveData;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.javarosa.core.model.data.IAnswerData;
import org.javarosa.core.reference.ReferenceManager;
import org.javarosa.form.api.FormEntryPrompt;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.controlelectoral.sialyasuni.R;
import org.controlelectoral.sialyasuni.audio.AudioButton;
import org.controlelectoral.sialyasuni.audio.AudioHelper;
import org.controlelectoral.sialyasuni.formentry.media.AudioHelperFactory;
import org.controlelectoral.sialyasuni.formentry.questions.QuestionDetails;
import org.controlelectoral.sialyasuni.injection.config.AppDependencyModule;
import org.controlelectoral.sialyasuni.support.CollectHelpers;
import org.controlelectoral.sialyasuni.support.MockFormEntryPromptBuilder;
import org.controlelectoral.sialyasuni.support.WidgetTestActivity;
import org.odk.collect.async.Scheduler;
import org.odk.collect.audioclips.Clip;

@RunWith(AndroidJUnit4.class)
public class QuestionWidgetTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    public AudioHelper audioHelper;

    @Before
    public void setup() throws Exception {
        overrideDependencyModule();
        when(audioHelper.setAudio(any(AudioButton.class), any())).thenReturn(new MutableLiveData<>());
    }

    @Test
    public void whenQuestionHasAudio_audioButtonUsesIndexAsClipID() throws Exception {
        FormEntryPrompt prompt = new MockFormEntryPromptBuilder()
                .withIndex("i am index")
                .withAudioURI("ref")
                .build();

        WidgetTestActivity activity = CollectHelpers.createThemedActivity(WidgetTestActivity.class);
        TestWidget widget = new TestWidget(activity, new QuestionDetails(prompt));

        AudioButton audioButton = widget.getAudioVideoImageTextLabel().findViewById(R.id.audioButton);
        verify(audioHelper).setAudio(audioButton, new Clip("i am index", "blah.mp3"));
    }

    private void overrideDependencyModule() throws Exception {
        ReferenceManager referenceManager = setupFakeReferenceManager(asList(new Pair<>("ref", "blah.mp3")));
        CollectHelpers.overrideAppDependencyModule(new AppDependencyModule() {

            @Override
            public ReferenceManager providesReferenceManager() {
                return referenceManager;
            }

            @Override
            public AudioHelperFactory providesAudioHelperFactory(Scheduler scheduler) {
                return context -> audioHelper;
            }
        });
    }

    private static class TestWidget extends QuestionWidget {

        TestWidget(Context context, QuestionDetails questionDetails) {
            super(context, questionDetails);
        }

        @Override
        public IAnswerData getAnswer() {
            return null;
        }

        @Override
        public void clearAnswer() {

        }

        @Override
        public void setOnLongClickListener(OnLongClickListener l) {

        }
    }
}
