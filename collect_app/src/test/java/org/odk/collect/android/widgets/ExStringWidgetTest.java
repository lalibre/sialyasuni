package org.controlelectoral.sialyasuni.widgets;

import androidx.annotation.NonNull;

import net.bytebuddy.utility.RandomString;

import org.javarosa.core.model.data.StringData;
import org.mockito.Mock;
import org.controlelectoral.sialyasuni.formentry.questions.QuestionDetails;
import org.controlelectoral.sialyasuni.widgets.base.GeneralExStringWidgetTest;
import org.controlelectoral.sialyasuni.widgets.support.FakeWaitingForDataRegistry;
import org.controlelectoral.sialyasuni.widgets.utilities.StringRequester;

import static org.mockito.Mockito.when;

/**
 * @author James Knight
 */

public class ExStringWidgetTest extends GeneralExStringWidgetTest<ExStringWidget, StringData> {

    @Mock
    StringRequester stringRequester;

    @NonNull
    @Override
    public ExStringWidget createWidget() {
        return new ExStringWidget(activity, new QuestionDetails(formEntryPrompt), new FakeWaitingForDataRegistry(), stringRequester);
    }

    @NonNull
    @Override
    public StringData getNextAnswer() {
        return new StringData(RandomString.make());
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        when(formEntryPrompt.getAppearanceHint()).thenReturn("");
    }
}
