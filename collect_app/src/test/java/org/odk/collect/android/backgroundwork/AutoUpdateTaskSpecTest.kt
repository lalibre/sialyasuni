package org.controlelectoral.sialyasuni.backgroundwork

import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.controlelectoral.sialyasuni.formmanagement.FormsUpdater
import org.controlelectoral.sialyasuni.formmanagement.matchexactly.SyncStatusAppState
import org.controlelectoral.sialyasuni.injection.config.AppDependencyModule
import org.controlelectoral.sialyasuni.notifications.Notifier
import org.controlelectoral.sialyasuni.projects.ProjectDependencyProviderFactory
import org.controlelectoral.sialyasuni.support.CollectHelpers

@RunWith(AndroidJUnit4::class)
class AutoUpdateTaskSpecTest {

    private val context = ApplicationProvider.getApplicationContext<Application>()
    private val formUpdateChecker = mock<FormsUpdater>()

    @Before
    fun setup() {
        CollectHelpers.overrideAppDependencyModule(object : AppDependencyModule() {
            override fun providesFormsUpdater(
                context: Context,
                notifier: Notifier,
                syncStatusAppState: SyncStatusAppState,
                projectDependencyProviderFactory: ProjectDependencyProviderFactory
            ): FormsUpdater {
                return formUpdateChecker
            }
        })
    }

    @Test
    fun `calls checkForUpdates with project from tag`() {
        val autoUpdateTaskSpec = AutoUpdateTaskSpec()
        val task = autoUpdateTaskSpec.getTask(context, mapOf(TaskData.DATA_PROJECT_ID to "projectId"), true)

        task.get()
        verify(formUpdateChecker).downloadUpdates("projectId")
    }

    @Test
    fun `maxRetries should not be limited`() {
        assertThat(AutoUpdateTaskSpec().maxRetries, `is`(nullValue()))
    }
}
